# chat_room

#### 介绍
仿微信单聊，群聊H5，通过gateway-worker搭建

#### 软件架构
软件架构说明


#### 安装教程
1.克隆项目到本地，cmd命令进入项目根目录
2.composer install 安装workerman,gateworker,think-worker等扩展库
3.windows点击start_for_win.bat linux使用php-cli运行start.php, 如: php start.php start（开启） php start.php stop（关闭）


#### 项目预览

![![输入图片说明](https://images.gitee.com/uploads/images/2019/0304/190355_7de3c19f_776822.jpeg "1551697402(1).jpg")](https://images.gitee.com/uploads/images/2019/0304/190317_4673651d_776822.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0301/195415_6a8cdda1_776822.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0304/190849_b080ac44_776822.jpeg "1551697706(1).jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0304/190935_a82a3185_776822.jpeg "1551697754(1).jpg")

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 公众号
![输入图片说明](https://images.gitee.com/uploads/images/2019/0312/125544_93354a30_776822.jpeg "164234_pf7y_2456768.jpg")

