<?php
namespace app\wap\controller;
use think\Controller;
use think\Session;

class Base extends Controller
{
    public function __construct() {
    	parent::__construct();
    	self::checkLogin();
    }

    public function checkLogin() {
    	$uid = Session::get('uid');
    	if(empty($uid)) {
    		return $this->error('请先登录','wap/Login/index');
    		//echo 'fuchao';
    	}
    }

}
