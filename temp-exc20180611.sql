/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : temp-exc20180611

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-03-04 18:46:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cicle
-- ----------------------------
DROP TABLE IF EXISTS `cicle`;
CREATE TABLE `cicle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `delete_id` int(11) NOT NULL DEFAULT '0',
  `delete_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cicle
-- ----------------------------
INSERT INTO `cicle` VALUES ('1', '小米手环', 'https://p0.ssl.qhimg.com/t01885df7ce5ed94246.jpg', '0', '0', '0');
INSERT INTO `cicle` VALUES ('2', '开发群', 'https://p0.ssl.qhimg.com/t01885df7ce5ed94246.jpg', '0', '0', '0');

-- ----------------------------
-- Table structure for cicle_user
-- ----------------------------
DROP TABLE IF EXISTS `cicle_user`;
CREATE TABLE `cicle_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cicle_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `delete_id` int(11) NOT NULL DEFAULT '0',
  `delete_time` int(11) NOT NULL,
  `is_main` int(1) NOT NULL DEFAULT '0' COMMENT '是否群主',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cicle_user
-- ----------------------------
INSERT INTO `cicle_user` VALUES ('1', '1', '1', '0', '0', '0', '1');
INSERT INTO `cicle_user` VALUES ('2', '2', '1', '0', '0', '0', '0');
INSERT INTO `cicle_user` VALUES ('3', '1', '3', '0', '0', '0', '1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(12) NOT NULL,
  `password` varchar(255) NOT NULL,
  `avator` varchar(255) NOT NULL,
  `realname` varchar(20) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `address` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'zerofc', 'e10adc3949ba59abbe56e057f20f883e', 'https://avatar.kancloud.cn/62/f582c3b46fb9d98e628e3e5535dd47', '零零', '15170897360', '江西吉安', '1741108471@qq.com', '1');
INSERT INTO `user` VALUES ('2', 'zfc', '654321 ', 'https://avatar.kancloud.cn/62/f582c3b46fb9d98e628e3e5535dd47', '一役', '18296653180', '吉安吉水', '6488878@qq.com', '1');
INSERT INTO `user` VALUES ('3', 'zero', 'e10adc3949ba59abbe56e057f20f883e', 'http://p0.qhimg.com/t01c3b56fd1f284f437.png', '笨小孩', '15189756248', '江西南昌', '4678864@qq.com', '1');
